package jv21701.bondarenko.test.multilevel.domain;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import org.hibernate.Session;

public abstract class AbstractDao<T, I extends Serializable> implements GenericDao<T, I> {

    private final Class<T> entityClass;

    protected Session getCurrentSession() {
        return HibernateUtil.getSessionFactory().openSession();
    }

    public AbstractDao() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass()
                .getGenericSuperclass();
        this.entityClass = (Class<T>) genericSuperclass
                .getActualTypeArguments()[0];
    }

    @Override
    public T getByID(I key) {
        Session session = getCurrentSession();
        return (T) session.load(entityClass, key);
    }

    @Override
    public T create(T object) {
        Session session = getCurrentSession();
        return (T) session.save(object);
    }

    @Override
    public boolean update(T object) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.update(object);
        return true;

    }

    @Override
    public boolean remove(T object) {
        Session session = getCurrentSession();
        session.delete(object);
        return true;
    }

    @Override
    public List<T> getList() {
        Session session = getCurrentSession();
        return (List<T>) session.createCriteria(entityClass).list();
    }

}
