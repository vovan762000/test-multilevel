package jv21701.bondarenko.test.multilevel.domain;

import java.util.List;

public interface CommentsDao extends GenericDao<Comments,Integer> {

    Comments getByID(int key);

    List<Comments> getList();
}
