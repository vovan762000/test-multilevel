package jv21701.bondarenko.test.multilevel.domain;

import java.util.List;
import org.hibernate.Session;

public class CommentsDaoImpl extends AbstractDao<Comments, Integer> implements CommentsDao {

    @Override
    public Comments getByID(int key) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        return (Comments) session.get(Comments.class, key);
    }

    @Override
    public List<Comments> getList() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        return session.createCriteria(Comments.class).list();
    }

}
