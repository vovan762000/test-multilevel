package jv21701.bondarenko.test.multilevel.domain;

import java.util.List;

public interface GenericDao<T, I> {

    T getByID(I key);

    T create(T o);

    boolean update(T o);

    boolean remove(T o);

    List<T> getList();

}
