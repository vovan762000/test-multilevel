package jv21701.bondarenko.test.multilevel.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "post_media")
@NamedQueries({
    @NamedQuery(name = "PostMedia.findAll", query = "SELECT p FROM PostMedia p")})
public class PostMedia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @Column(name = "id_post")
    private int idPost;
    
    @Basic(optional = false)
    @Column(name = "id_media")
    private int idMedia;

    public PostMedia() {
    }

    public PostMedia(Integer id) {
        this.id = id;
    }

    public PostMedia(Integer id, int idPost, int idMedia) {
        this.id = id;
        this.idPost = idPost;
        this.idMedia = idMedia;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdPost() {
        return idPost;
    }

    public void setIdPost(int idPost) {
        this.idPost = idPost;
    }

    public int getIdMedia() {
        return idMedia;
    }

    public void setIdMedia(int idMedia) {
        this.idMedia = idMedia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PostMedia)) {
            return false;
        }
        PostMedia other = (PostMedia) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jv21701.bondarenko.test.multilevel.domain.PostMedia[ id=" + id + " ]";
    }

}
