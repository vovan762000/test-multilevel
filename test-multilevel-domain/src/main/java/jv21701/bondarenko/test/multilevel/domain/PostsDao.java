package jv21701.bondarenko.test.multilevel.domain;

import java.util.List;

public interface PostsDao extends GenericDao<Posts,Integer> {

    public List<Comments> getCommentsList(Posts posts);

}
