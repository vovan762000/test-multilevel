package jv21701.bondarenko.test.multilevel.domain;

import java.util.List;


public class PostsDaoImpl extends AbstractDao<Posts,Integer> implements PostsDao {

    @Override
    public List<Comments> getCommentsList(Posts posts) {
        return posts.getCommentsList();
    }

}
