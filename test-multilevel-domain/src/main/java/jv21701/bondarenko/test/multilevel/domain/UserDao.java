package jv21701.bondarenko.test.multilevel.domain;

public interface UserDao extends GenericDao<Users, Integer> {

    Users getByUsername(String username);
}
