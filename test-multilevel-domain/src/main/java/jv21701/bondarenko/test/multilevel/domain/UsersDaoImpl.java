package jv21701.bondarenko.test.multilevel.domain;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

public class UsersDaoImpl extends AbstractDao<Users, Integer> implements UserDao {

    public List<Posts> getPostsList(Users user) {
        return user.getPostsList();
    }

    @Override
    public Users getByUsername(String nickname) {
        Criteria criteria = getCurrentSession().createCriteria(Users.class)
                .add(Restrictions.eq("firstname", nickname));
        List<Users> users = criteria.list();
        return (users.size() > 0) ? users.get(0) : null;
    }

}
