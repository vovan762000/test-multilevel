package jv21701.bondarenko.test.multilevel.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "userslike")
@NamedQueries({
    @NamedQuery(name = "Userslike.findAll", query = "SELECT u FROM Userslike u")})
public class Userslike implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "posts_id")
    private int postsId;
    @Basic(optional = false)
    @Column(name = "comments_id")
    private int commentsId;
    @Basic(optional = false)
    @Column(name = "likecount")
    private int likecount;

    public Userslike() {
    }

    public Userslike(Integer id) {
        this.id = id;
    }

    public Userslike(Integer id, int postsId, int commentsId, int likecount) {
        this.id = id;
        this.postsId = postsId;
        this.commentsId = commentsId;
        this.likecount = likecount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getPostsId() {
        return postsId;
    }

    public void setPostsId(int postsId) {
        this.postsId = postsId;
    }

    public int getCommentsId() {
        return commentsId;
    }

    public void setCommentsId(int commentsId) {
        this.commentsId = commentsId;
    }

    public int getLikecount() {
        return likecount;
    }

    public void setLikecount(int likecount) {
        this.likecount = likecount;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Userslike)) {
            return false;
        }
        Userslike other = (Userslike) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jv21701.bondarenko.test.multilevel.domain.Userslike[ id=" + id + " ]";
    }

}
